const Discord = require('discord.js');
const client = new Discord.Client;
const fs = require("fs");
const wikihow = require('how-to-what');
let contents = fs.readFileSync('./auth.json');
let token = JSON.parse(contents);

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    if (msg.member !== null) {
        if (msg.member.user.id !== client.user.id) {
            if (msg.isMentioned(client.user)) {
                if (msg.content.search("help") !== -1) {
                    msg.channel.send(`I dont think i can help you <@${msg.member.user.id}>`)
                }
            }


            //idk module
            let random = Math.floor((Math.random() * 6) + 1);
            let quesloc = -1;
            let ques = -1;

            if (msg.content.search("[Ww][Hh][Oo]") !== -1) {
                quesloc = msg.content.search("[Ww][Hh][Oo]");
                ques = 1;
            }

            if (msg.content.search("[Ww][Hh][Aa][Tt]") !== -1) {
                quesloc = msg.content.search("[Ww][Hh][Aa][Tt]");
                ques = 2;
            }

            if (msg.content.search("[Ww][Hh][Ee][Rr][Ee]") !== -1) {
                quesloc = msg.content.search("[Ww][Hh][Ee][Rr][Ee]");
                ques = 3;
            }

            if (msg.content.search("[Ww][Hh][Ee][Nn]") !== -1) {
                quesloc = msg.content.search("[Ww][Hh][Ee][Nn]");
                ques = 4;
            }

            if (msg.content.search("[Ww][Hh][Yy]") !== -1) {
                quesloc = msg.content.search("[Ww][Hh][Yy]");
                ques = 5;
            }

            if (msg.content.search("[Hh][Oo][Ww]") !== -1) {
                quesloc = msg.content.search("[Hh][Oo][Ww]");
                ques = 6;
            }


            if (quesloc !== -1 && ques !== -1) {
                let msgSub = msg.content.substr(quesloc, msg.content.length);
                switch (random) {
                    case 1: {
                        let msgSubFrmt = msgSub.replace(/[ ]/g, "+");
                        let providers = ['g', 'b', 'y', 'a', 'k', 'd'];
                        let provider = providers[Math.floor(Math.random()*providers.length)];
                        msg.channel.send(`http://lmgtfy.com/?s=${provider}&q=${msgSubFrmt}`);
                        break
                    }
                    case 2: {
                        msg.channel.send(`With questions like \"${msgSub}\" I don't think you really belong here ${msg.member.user}`);
                        break
                    }
                    case 3: {
                        msg.channel.send(`install gentoo ${msg.member.user}`);
                        break
                    }
                    case 4: {
                        wikihow.howTo(msgSub)
                            .then((response) => {
                                msg.channel.send(response);
                            })
                            .catch(console.error);
                        break
                    }
                    case 5: {
                        msg.channel.send(`${msg.member.user} i dont know`);
                        break
                    }
                    case 6: {
                        msg.channel.send(`are you sure you are alright ${msg.member.user}?`);
                        break
                    }

                }
            }

            //Dad module
            let imFind = /i'?m\s+(.*)/i;
            let matches = imFind.exec(msg.content);
            if (matches !== null) {
                msg.channel.send(`Hi ${matches[1]}, I'm dad!`);
            }
            let areFind = /are\s+you\s+(.*)/i;
            let areyoumatches = areFind.exec(msg.content);
            if (areyoumatches !== null){
                msg.channel.send(`No, I'm Dad.`)
            }
        }
    }

});

client.on('presenceUpdate', async (oldMember, newMember) => {
    let game = newMember.presence.game;
    const delay = (msec) => new Promise((resolve) => setTimeout(resolve, msec));
    if (game !== null) {
        game.name = game.name.toString();
        if (game.name === "Fortnite") {
            newMember.send("No Fortnite allowed, You have been banned permanently.");
            client.channels.get("424341375115591703").send(`@everyone ${oldMember.user.tag} has been banned for playing Fortnite.`).catch();
            await delay(500);
            newMember.ban("No Fortnite allowed");
        }
    }
});

client.login(token.authtoken);